-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Jeu 03 Janvier 2019 à 18:57
-- Version du serveur :  5.6.20-log
-- Version de PHP :  5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `projet tut`
--

-- --------------------------------------------------------

--
-- Structure de la table `administrateur`
--

CREATE TABLE IF NOT EXISTS `administrateur` (
  `idAdministrateur` int(11) NOT NULL,
  `username` text NOT NULL,
  `password` text NOT NULL,
  `droit` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `administrateur`
--

INSERT INTO `administrateur` (`idAdministrateur`, `username`, `password`, `droit`) VALUES
(0, 'root1', 'root1', 0),
(1, 'root2', 'root2', 1);

-- --------------------------------------------------------

--
-- Structure de la table `difficulte`
--

CREATE TABLE IF NOT EXISTS `difficulte` (
  `idDifficulte` int(11) NOT NULL,
  `vitesse` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `difficulte`
--

INSERT INTO `difficulte` (`idDifficulte`, `vitesse`) VALUES
(0, 0),
(1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `musique`
--

CREATE TABLE IF NOT EXISTS `musique` (
  `idMusique` int(11) NOT NULL,
  `nom` text NOT NULL,
  `artiste` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `musique`
--

INSERT INTO `musique` (`idMusique`, `nom`, `artiste`) VALUES
(0, 'musique1', 'Jul'),
(1, 'musique2', 'Gradur');

-- --------------------------------------------------------

--
-- Structure de la table `notes`
--

CREATE TABLE IF NOT EXISTS `notes` (
  `idNotes` int(11) NOT NULL,
  `contenu` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `notes`
--

INSERT INTO `notes` (`idNotes`, `contenu`) VALUES
(0, '11 points'),
(1, '3 points');

-- --------------------------------------------------------

--
-- Structure de la table `partie`
--

CREATE TABLE IF NOT EXISTS `partie` (
  `idPartie` int(11) NOT NULL,
  `nomPartie` text NOT NULL,
  `type` text NOT NULL,
  `nomSponsor` text,
  `token` text,
  `cadeau` text,
  `nbMinJoueur` int(11) NOT NULL DEFAULT '0',
  `idMusique` int(11) NOT NULL,
  `idNotes` int(11) NOT NULL,
  `idDifficulte` int(11) NOT NULL,
  `idAdministrateur` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `partie`
--

INSERT INTO `partie` (`idPartie`, `nomPartie`, `type`, `nomSponsor`, `token`, `cadeau`, `nbMinJoueur`, `idMusique`, `idNotes`, `idDifficulte`, `idAdministrateur`) VALUES
(0, 'partie 0', 'normale', NULL, '0', NULL, 0, 0, 0, 0, 0),
(1, 'partie 1', 'sponso', 'darty', '1', 'une pentade', 5, 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `score`
--

CREATE TABLE IF NOT EXISTS `score` (
  `idScore` int(11) NOT NULL,
  `nomJoueur` text NOT NULL,
  `score` int(11) NOT NULL DEFAULT '0',
  `idPartie` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `score`
--

INSERT INTO `score` (`idScore`, `nomJoueur`, `score`, `idPartie`) VALUES
(0, 'Jean', 55, 1);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `administrateur`
--
ALTER TABLE `administrateur`
 ADD PRIMARY KEY (`idAdministrateur`);

--
-- Index pour la table `difficulte`
--
ALTER TABLE `difficulte`
 ADD PRIMARY KEY (`idDifficulte`);

--
-- Index pour la table `musique`
--
ALTER TABLE `musique`
 ADD PRIMARY KEY (`idMusique`);

--
-- Index pour la table `notes`
--
ALTER TABLE `notes`
 ADD PRIMARY KEY (`idNotes`);

--
-- Index pour la table `partie`
--
ALTER TABLE `partie`
 ADD PRIMARY KEY (`idPartie`), ADD KEY `idAdministrateur` (`idAdministrateur`), ADD KEY `idDifficulte` (`idDifficulte`), ADD KEY `idNotes` (`idNotes`), ADD KEY `idMusique` (`idMusique`);

--
-- Index pour la table `score`
--
ALTER TABLE `score`
 ADD PRIMARY KEY (`idScore`), ADD KEY `idPartie` (`idPartie`);

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `partie`
--
ALTER TABLE `partie`
ADD CONSTRAINT `partie_ibfk_1` FOREIGN KEY (`idMusique`) REFERENCES `musique` (`idMusique`),
ADD CONSTRAINT `partie_ibfk_2` FOREIGN KEY (`idNotes`) REFERENCES `notes` (`idNotes`),
ADD CONSTRAINT `partie_ibfk_3` FOREIGN KEY (`idDifficulte`) REFERENCES `difficulte` (`idDifficulte`),
ADD CONSTRAINT `partie_ibfk_4` FOREIGN KEY (`idAdministrateur`) REFERENCES `administrateur` (`idAdministrateur`);

--
-- Contraintes pour la table `score`
--
ALTER TABLE `score`
ADD CONSTRAINT `fk_Score` FOREIGN KEY (`idPartie`) REFERENCES `partie` (`idPartie`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

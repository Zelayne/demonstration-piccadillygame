# README #

Ce README a pour but d'expliquer comment installer l'application et de fournir les documents et liens utiles pour le projet

### Comment installer la base de données ? ###

* Cette base de données utilise le SGBD MySQL
* Importer le fichier "Base de Données/bdd_projet_tut.sql" 

### Où se trouvent les documents ? ###

* Les différents rapports d'analyse se trouvent dans le dossier /docs/
* Les diagrammes se trouvent dans le dossier /diagrammes/

### Liens utiles ###

* [Trello](https://trello.com/b/5lqaftLI/s3ds15adjaoudgenyhennardspatz)


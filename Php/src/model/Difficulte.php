<?php
/**
 * Created by PhpStorm.
 * User: MSI-Sofiane
 * Date: 28/12/2018
 * Time: 11:46
 */
namespace piccadilly\model;
class Difficulte extends \Illuminate\Database\Eloquent\Model {

    protected $table = 'difficulte';
    protected $primaryKey = 'idDifficulte';
    public $timestamps = false;
}
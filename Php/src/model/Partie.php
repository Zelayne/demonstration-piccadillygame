<?php
/**
 * Created by PhpStorm.
 * User: MSI-Sofiane
 * Date: 28/12/2018
 * Time: 11:48
 */
namespace piccadilly\model;
class Partie extends \Illuminate\Database\Eloquent\Model {

    protected $table = 'partie';
    protected $primaryKey = 'idPartie';
    public $timestamps = false;
}
<?php
/**
 * Created by PhpStorm.
 * User: MSI-Sofiane
 * Date: 28/12/2018
 * Time: 11:44
 */

namespace piccadilly\model;
class Administrateur extends \Illuminate\Database\Eloquent\Model {

    protected $table = 'administrateur';
    protected $primaryKey = 'idAdministrateur';
    public $timestamps = false;

}
<?php
/**
 * Created by PhpStorm.
 * User: MSI-Sofiane
 * Date: 28/12/2018
 * Time: 11:47
 */
namespace piccadilly\model;
class Notes extends \Illuminate\Database\Eloquent\Model {

    protected $table = 'notes';
    protected $primaryKey = 'idNotes';
    public $timestamps = false;
}
<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 20/11/2018
 * Time: 21:01
 */

namespace piccadilly\vues;


use piccadilly\Utils\Alerte;
use Slim\Slim;
require_once ('vendor/autoload.php');
class VuePageHTMLBootStrap
{

    public function renderTop(){
        $racine=BASE_URL;
        $html='

        
        <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Piccadilly Game</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="'.$racine.'/css/bootstrap.min.css">

        <link rel="icon" href="'.$racine.'favicon.ico" />

        <!--For Plugins external css-->
        <link rel="stylesheet" href="'.$racine.'/css/plugins.css" />
        <link rel="stylesheet" href="'.$racine.'/css/lora-web-font.css" />
        <link rel="stylesheet" href="'.$racine.'/css/opensans-web-font.css" />
        <link rel="stylesheet" href="'.$racine.'/css/magnific-popup.css">
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8/jquery.min.js"></script>
         <script type="text/javascript" src="'.$racine.'/js/userActions.js"></script>
         <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

        <!--Theme custom css -->
        <link rel="stylesheet" href="'.$racine.'/css/style.css">

        <!--Theme Responsive css-->
        <!--<link rel="stylesheet" href="'.$racine.'/css/responsive.css" />-->
        
        
        </head>
       
        ';


        return $html;
        
    }
    public function renderAlerts(){
        $html = "";
        foreach(Alerte::getAll() as $type => $tab)
        {
            foreach($tab as $message) {
                $html .= '<div class="fade in alert-dismissible alert alert-';
                switch($type) {
                    case 'success':
                        $html .= 'success';
                        break;
                    case 'info':
                        $html .= 'info';
                        break;
                    case 'error':
                        $html .= 'danger';
                        break;
                    case 'warning':
                        $html .= 'warning';
                        break;
                }
                $html .= '"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>'.$message.'</div>';
            }
        }
        Alerte::clear();
        return $html;


    }
    public function renderBottom(){
       return <<<HTML
       <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
            <div id="footer" class="footer">
                <div class="copyright ">
                    <p class="text-center">&copy; 2018 - Créé dans le cadre du projet tutoré</p>
                </div>
            </div>
        </div>
    </html>
HTML;
    }

}
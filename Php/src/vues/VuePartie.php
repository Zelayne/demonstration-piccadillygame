<?php
/**
 * Created by PhpStorm.
 * User: MSI-Sofiane
 * Date: 29/12/2018
 * Time: 09:37
 */
namespace piccadilly\vues;
class VuePartie extends AbstractView
{

    function render(){
       $html = <<<END
        <script type="text/javascript">
            var timer=0;
            var mousedown=1;
            function displayTimer() {
                if (mousedown==1) {
                     mousedown = setInterval(displayTimer, 10);
                }
                timer+=0.01;
                document.getElementById("demo").innerHTML = timer.toPrecision(3);
            }

            function stopTimer(idbtn) {
                clearInterval(mousedown);
                mousedown=1;
                timer=0;
            }
            
            
            function preventLongPressMenu(node) {
                node.on('touchstart', absorbEvent_);
                node.on('touchmove', absorbEvent_);
                node.on('touchend', absorbEvent_);
                node.on('touchcancel', absorbEvent_);
            }
            function init() {
                preventLongPressMenu($('body img'));
            }

            function actions(text) {
              
            }
            
            function absorbEvent_(event) {
              var e = event || window.event;
              e.preventDefault && e.preventDefault();
              e.stopPropagation && e.stopPropagation();
              e.cancelBubble = true;
              e.returnValue = false;
              return false;
            }
            
        </script>
        
         <style>
            *:not(input):not(textarea) {
                 -webkit-user-select: none; /* disable selection/Copy of UIWebView */
                 -webkit-touch-callout: none; /* disable the IOS popup when long-press on a link */
             }
          </style>
</head>
<body>

</body>
END;




        if (isset($_COOKIE["pseudo"])) {
            $pseudo = $_COOKIE["pseudo"];


           $html.= <<<END
                     <body onload="init()">
                         <div class="col-md-12">
                             <button style="width: 24%; height: 100%; background: red" value="1" id="1" class="btnJeu" >1</button>
                             <button style="width: 24%; height: 100%; background: blue" value="1" id="2" class="btnJeu" >2</button>
                             <button style="width: 24%; height: 100%; background: #2b542c" value="1" id="3" class="btnJeu" >3</button>
                             <button style="width: 24%; height: 100%; background: yellow" value="1" id="4" class="btnJeu" >4</button>
                         </div>
                    <p id="demo"></p>
                    <script>
                    var xhr = new XMLHttpRequest();
                    xhr.open('GET', "http://localhost:8080/ProjetTut/1/partie");
                    xhr.send("Yo");
                    $('.btnJeu').on('touchstart touchmove mousedown', function(e){
                        e.preventDefault();
                        e.stopPropagation();
                        displayTimer();
                
                     });
            
                    $('.btnJeu').on('touchend touchcancel mouseup click', function(e){
                        e.preventDefault();
                        e.stopPropagation();
                        stopTimer();
                       
                    });
        
</script>
        
END;
        } else {
           $html.= <<<END

        <body>
            <p id="actions">xhr.responseText</p>
END;
        }
    return $html;
    }

}
<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 05/01/2019
 * Time: 16:38
 */

namespace piccadilly\vues;

class VueAfficherPartie extends AbstractView {

    /**
     * Methode permettant d'afficher la vue
     * @return mixed
     */
    protected function render()
    {
        $url = BASE_URL;
       return <<<END
        </head>
        
        <script>
            var url = $url;
            var exampleSocket = new WebSocket(url);
           function sendAction(pseudo, numBouton, temps, url) {
                 $.get(url, function(html){
                 alert(html.toString());
              });
        </script>
        
        <p id="actions">Aucune action n'a été effectuée</p>
        
END;

    }
}
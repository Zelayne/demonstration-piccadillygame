<?php
/**
 * Created by PhpStorm.
 * User: MSI-Sofiane
 * Date: 28/12/2018
 * Time: 11:54
 */
namespace piccadilly\vues;
use Slim\Slim;

class VueSalon extends AbstractView
{

    /**
     * Methode permettant d'afficher la vue
     * @return mixed
     */
    protected function render()
    {
        $app = Slim::getInstance();
        return <<<END
               <header id="home" class="home">
                    <div class="overlay ">
                        <div class="container-fluid">
              
                            <div class="home-wrapper">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                  <div class="home-content text-center">
                                      <h1>Piccadilly game</h1>
                                      <h4>Picadilly game est un jeu de rythme sur navigateur web accessible pour smartphone. Cliquez sur les bonnes touches en rythme avec la musique et tentez de gagner des avantages de nos partenaires. </h4>
                                  </div>
                                </div>
                            </div>
                        </div>
                    </div>
               </header>
           
                    <row>
                        <div class="col-md-3 col-md-offset-1">
                            <form action="" method="post">
                                <input type="text" name="pseudo"  placeholder="Entrez un pseudo" required="required" />
                               <input type="submit" value="Accéder">
                            </form>
                        </div>
                    </row>
       
 


END;
    }
}

?>



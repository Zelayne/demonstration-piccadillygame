<?php
/**
 * Created by PhpStorm.
 * User: MSI-Sofiane
 * Date: 28/12/2018
 * Time: 11:54
 */
namespace piccadilly\vues;


use Slim\Slim;

class VueAccueil extends AbstractView
{

    public function render(){
        $racine = BASE_URL;
        $app = Slim::getInstance();
        $menu = '<li><a href="#">Accueil</a></li>';
        $menu .= '<li><a href="#">Espace Administrateur</a></li>';
        $menu .= '<li><a href="'.$app->urlFor('salon', ['token' => 1]).'">Lancer une partie</a></li>';
/**
     //---------Partie Connexion-------------------------------------------------------------
        $menu .= '<li><a href="#"><div class="row"><form class="form-inline">';
        $menu .= '<div class="form-group"><label for="identifiant">Identifiant : </label>';
        $menu .= '<input type="text" id="identifiant" class="form-control"></div>';
        $menu .= '<div class="form-group"><label for="mdp">Mot de passe : </label>';
        $menu .= '<input type="text" id="mdp" class="form-control"></div>';
        $menu .= '<button type="submit" class="btn btn-default">Valider</button>';
        $menu .='</form></div></a></li>';
     //--------------------------------------------------------------------------------------
**/

        return <<<END
        <nav class="navbar navbar-default navbar-fixed-top" id="main_navbar">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Menu</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#"><img src="$racine/img/logo.png" alt="logo" /></a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        $menu
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>
        <header id="home" class="home">
            <div class="overlay ">
                <div class="container-fluid">
              
                        <div class="home-wrapper">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="home-content text-center">
                                    <h1 class=>Piccadilly Game</h1>
                                    <h4>Soyez en rythme sur des musiques endiablées !</h4>
                                </div>
                            </div>
                        </div>
                
                </div>
            </div>
         
        </header>
         <section id="service" class="service sections">
            <div class="container">
                <div class="heading text-center">
                    <h1>Nos services</h1>
                    <div class="separator"></div>
                </div>
                <!-- Example row of columns -->
                <div class="row">
                    <div class="wrapper">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="service-item text-center">
                                <i class="fas fa-music"></i>
                                <h5>Jouez sur vos musiques</h5>
                                <div class="separator2"></div>
                                <p>Jouez sur vos musiques préférées et tapez en rythme !</p>
                            </div>
                        </div> 

                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="service-item text-center">
                                <i class="fas fa-pen-alt"></i>
                                <h5>Créez vos parties</h5>
                                <div class="separator2"></div>
                                <p>Créez vos propres parties qui seront uniques !</p>
                            </div>
                        </div> 
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="service-item text-center">
                                <i class="fas fa-user-friends"></i>
                                <h5>Affrontez vos amis</h5>
                                <div class="separator2"></div>
                                <p>Affrontez vos amis pour savoir lequel est le plus en rythme !</p>
                            </div>
                        </div> 
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="service-item text-center">
                                <i class="fas fa-trophy"></i>
                                <h5>Gagnez des prix</h5>
                                <div class="separator2"></div>
                                <p>Soyez le meilleur et tentez de gagner des prix de nos partenaires !</p>
                            </div>
                        </div> 
                    </div>
                </div>
            </div> <!-- /container -->       
        </section>
     
END;

    }
}
<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 22/11/2018
 * Time: 17:11
 */

namespace piccadilly\Utils;

use Slim\Slim;

class Alerte
{
    const ERROR = 0;
    const SUCCESS = 1;
    const WARNING = 2;
    const INFO = 3;

    private static $nb = [0, 0, 0, 0];
    private static $currentAlerts = [];
    private static $hasCatchAlerts = false;

    public static function create($message, $type)
    {
        $_SESSION['alert-manager'][$type][self::$nb[$type]] = $message;
        self::$nb[$type] += 1;
    }

    public static function createThenRedirect($message, $type, $nameRoute, $paramRoute = [])
    {
        self::create($message, $type);
        (Slim::getInstance())->redirectTo($nameRoute, $paramRoute);
    }

    public static function getAll()
    {
        if (!self::$hasCatchAlerts && isset($_SESSION['alert-manager'])) {
            $result = ['info' => [], 'success' => [], 'warning' => [], 'error' => []];
            foreach ($_SESSION['alert-manager'] as $type => $tab) {
                foreach ($tab as $message) {
                    if ($type == Alerte::ERROR)
                        $result['error'][] = $message;
                    if ($type == Alerte::SUCCESS)
                        $result['success'][] = $message;
                    if ($type == Alerte::WARNING)
                        $result['warning'][] = $message;
                    if ($type == Alerte::INFO)
                        $result['info'][] = $message;
                }
            }
            self::$currentAlerts = $result;
        }

        return self::$currentAlerts;
    }

    public static function clear()
    {
        $_SESSION['alert-manager'] = [];
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 05/01/2019
 * Time: 12:56
 */

namespace piccadilly\controleur;


use piccadilly\vues\VueAfficherPartie;
use piccadilly\vues\VuePartie;
use piccadilly\vues\VueSalon;
use Slim\Slim;

class ControleurJeu
{

    public function accederSalon($token) {
        echo (new VueSalon())->renderAll(["token"=>$token]);
    }

    public function accederPartie($token) {
        if (isset($_POST['pseudo'])) {
            setcookie('pseudo', $_POST['pseudo']);
        }
        Slim::getInstance()->redirectTo('partie', ['token' =>$token]);
    }

    public function afficherPanel($token) {
        echo (new VuePartie())->renderAll(["token"=>$token]);
    }

    public function afficherPartie($token) {
        echo (new VueAfficherPartie())->renderAll(["token"=>$token]);
    }

}
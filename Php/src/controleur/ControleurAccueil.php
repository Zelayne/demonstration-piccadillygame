<?php
/**
 * Created by PhpStorm.
 * User: MSI-Sofiane
 * Date: 30/12/2018
 * Time: 14:51
 */

namespace piccadilly\controleur;

use piccadilly\vues\VueAccueil;

class ControleurAccueil
{
    /**
     * Afficher la page d'acceuil
     */
    public function displayHome(){
        echo (new VueAccueil())->renderAll();
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: MSI-Sofiane
 * Date: 29/12/2018
 * Time: 09:17
 */
namespace piccadilly\controleur;
use \picadilly_php\model\Partie as Partie;
use \picadilly_php\vue\VuePartie as VuePartie;
use Illuminate\Database\QueryException;
class ControleurPartie
{
    /**
     * Créer une partie
     */
    function createItem()
    {
        $view = new VuePartie();
        $partie = new Partie();

        // Vérifie les données envoyées
        if (!isset($_POST['nomPartie'])) $view->error("Veuillez entrer un nom de partie");
        if (!isset($_POST['type'])) $view->error("Veuillez entrer un type de partie");
        if (!isset($_POST['token'])) $view->error("Le token est manquant");
        if (!isset($_POST['nbMinJoueur'])) $view->error("Veuiller entrer un nombre minimum de joueurs");
        if (!isset($_POST['idMusique'])) $view->error("id de musique manquant");
        if (!isset($_POST['idDifficulte'])) $view->error("Veuiller séléctionner une difficulté");
        if (!isset($_POST['idAdministrateur'])) $view->error("Veuiller saisir un id d'administrateur");
        //pour l'instant nbMinJoueur > 2
        if (strlen($_POST['nbMinJoueur']) < 2) $view->error("Le nombre minimum de joueur est 2");

        // Récupère les données
        $nom = filter_var($_POST['nomPartie'], FILTER_SANITIZE_STRING);
        $type = filter_var($_POST['type'], FILTER_SANITIZE_STRING);
        $token = filter_var($_POST['token'],FILTER_SANITIZE_STRING);
        $nbMinJoueur = filter_var($_POST['nbMinJoueur'],FILTER_SANITIZE_STRING);
        $idMusique = filter_var($_POST['idMusique'],FILTER_SANITIZE_STRING);
        $idDifficulte = filter_var($_POST['idDifficulte'],FILTER_SANITIZE_STRING);
        $idAdministrateur = filter_var($_POST['idAdministrateur'],FILTER_SANITIZE_STRING);
        $url = isset($_POST['partieUrl']) ? filter_var($_POST['partieUrl'], FILTER_SANITIZE_URL) : '';


        // Ajoute le http:// si besoin
        if (strlen($url) != 0 && strpos($url, 'http://') === false)
            $url = 'http://' . $url;

        // Sauvegarde les données
        $partie->nom = $nom;
        $partie->type = $type;
        $partie->token = $token;
        $partie->nbMinJoueur = $nbMinJoueur;
        $partie->idMusique = $idMusique;
        $partie->idDifficulte = $idDifficulte;
        $partie->idAdministrateur = $idAdministrateur;
        $partie->url = $url;

        //pour l'instant
        $partie->nomSponsor = NULL;
        $partie->cadeau = NULL;
        $partie->idNotes = NULL;


        $partie->token = stripslashes(crypt($partie->nom, 'sel de mer'));


        try {
            $partie->save();
            $view->addHeadMessage("La partie a bien été enregistré", 'good');
            $view->renderPartie($partie);
        } catch (QueryException $e) {
            $view->addHeadMessage("Erreur lors de l'enregistrement", 'bad');
            $view->renderPartie($partie);
        }

    }

    /**
     * Supprimer une partie
     * @param  $idItem    L'id de la partie à supprimer
     * @param  $tokenItem Le token de la partie à supprimer
     */
    function delItem($idPartie, $tokenPartie) {
        $view = new VuePartie();
        $partie = Partie::where(['idPartie' => $idPartie , 'token' => $tokenPartie])->first();


        if (!isset($partie))
            $view->error('Partie inexistant');



        if (isset($partie) && $partie->delete()) {
            $view->addHeadMessage('Votre item a bien été supprimé', 'good');
            $view->renderPartie($partie);
        } else {
            $view = new VuePartie();
            $view->addHeadMessage("Votre item n'a pas pu être supprimé", 'bad');
            $view->renderPartie($partie);
        }
    }


}
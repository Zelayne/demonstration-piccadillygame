<?php

include 'src/conf/config.php';
/**
 * Created by PhpStorm.
 * Date: 30/12/2018
 * Time: 15:01
 */

require_once 'vendor/autoload.php';
use Illuminate\Database\Capsule\Manager as DB;
use piccadilly\controleur as c;


session_start();

$db = new DB();
$db->addConnection(parse_ini_file('src/conf/conf.ini'));
$db->bootEloquent();

$app = new \Slim\Slim();


//afficher la page d'acceuil
$app->get('/', function() {
    (new c\ControleurAccueil())->displayHome();
})->name('root');

$app->get('/:token/salon', function($token)  {
    (new c\ControleurJeu())->accederSalon($token);
})->name('salon');

$app->post('/:token/salon', function($token) {
    (new c\ControleurJeu())->accederPartie($token);
})->name('partieAccess');

$app->get('/:token/partie', function($token){
    (new c\ControleurJeu())->afficherPanel($token);
})->name('partie');

$app->get('/:token/display', function ($token) {
    (new c\ControleurJeu())->afficherPartie($token);
})->name('show');






$app->run();

